const palindrome = require('./palindrome');

describe('Test Palindrome', ()=>{
	it('checks', () => {
		expect(palindrome.isPalindrome("di")).toBe(false);
		expect(palindrome.isPalindrome("did")).toBe(true);
		expect(palindrome.isPalindrome("hannah")).toBe(true);
		expect(palindrome.isPalindrome("level")).toBe(true);
		expect(palindrome.isPalindrome("kayak")).toBe(true);
		expect(palindrome.isPalindrome("kayaks")).toBe(false);
	});
})
