const {getElementThatRepeatsEvenTimes} = require('./element-repeats-even-times');

describe('Test getElementThatRepeatsEvenTimes', ()=>{
	it('checks', () => {
        expect(getElementThatRepeatsEvenTimes(["a", "a", "a","a","b"])).toBe("a");
        expect(getElementThatRepeatsEvenTimes(["a", "a", "b","a","b"])).toBe("b");
	});
})
